/* tslint:disable no-var-requires */
const withReadme = (require("storybook-readme/with-readme") as any).default;
const DynamicGridColumnReadme = require("./README.md");

import * as React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import DynamicGridColumn from "./DynamicGridColumn";

storiesOf("DynamicGridColumn", module)
  .addDecorator(withReadme(DynamicGridColumnReadme))
  .add("default", () => {
    const icon: foo = null; // TODO assign real value

    return (
      <DynamicGridColumn icon={icon} />
    );
  });
