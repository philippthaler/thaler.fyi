import { render, configure } from "enzyme";
import "jest";
import * as React from "react";
import DynamicGridColumn from "./DynamicGridColumn";

// Configure enzyme with react 16 adapter
const Adapter: any = require("enzyme-adapter-react-16");
configure({ adapter: new Adapter() });

describe("DynamicGridColumn component", () => {
  it("should render correctly", () => {
    const icon: foo = null; // TODO assign real value

    const wrapper = render(<DynamicGridColumn icon={icon} />);
    expect(wrapper).toMatchSnapshot();
  });
});
