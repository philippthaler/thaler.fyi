import * as React from "react";
import {
  Grid,
  Header,
  Icon,
  SemanticICONS,
  SemanticCOLORS,
} from "semantic-ui-react";

interface DynamicGridColumnProps {
  iconName: SemanticICONS;
  title: string;
  content: JSX.Element;
  link?: string;
  secondaryColor?: SemanticCOLORS;
}

const DynamicGridColumn = ({
  iconName,
  title,
  content,
  link,
  secondaryColor = "teal",
}: DynamicGridColumnProps) => {
  const [isHovering, setIsHovering] = React.useState(false);

  return (
    <Grid.Column as="a" href={link}>
      <div
        onMouseEnter={() => setIsHovering(true)}
        onMouseLeave={() => setIsHovering(false)}
      >
        <Header icon color={isHovering ? secondaryColor : "black"}>
          <Icon name={iconName}></Icon>
          {title}
        </Header>
        <div
          style={{
            color: isHovering ? secondaryColor : "black",
          }}
        >
          {content}
        </div>
      </div>
    </Grid.Column>
  );
};

export default DynamicGridColumn;
