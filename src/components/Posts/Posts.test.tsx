import { render, configure } from "enzyme";
import "jest";
import * as React from "react";
import Posts from "./Posts";

// Configure enzyme with react 16 adapter
const Adapter: any = require("enzyme-adapter-react-16");
configure({ adapter: new Adapter() });

describe("Posts component", () => {
  it("should render correctly", () => {
    const wrapper = render(<Posts />);
    expect(wrapper).toMatchSnapshot();
  });
});
