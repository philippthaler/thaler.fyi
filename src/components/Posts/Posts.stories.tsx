/* tslint:disable no-var-requires */
const withReadme = (require("storybook-readme/with-readme") as any).default;
const PostsReadme = require("./README.md");

import * as React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import Posts from "./Posts";

storiesOf("Posts", module)
  .addDecorator(withReadme(PostsReadme))
  .add("default", () => (
    <Posts />
  ));
