import { render, configure } from "enzyme";
import "jest";
import * as React from "react";
import LinkIcon from "./LinkIcon";

// Configure enzyme with react 16 adapter
const Adapter: any = require("enzyme-adapter-react-16");
configure({ adapter: new Adapter() });

describe("LinkIcon component", () => {
  it("should render correctly", () => {
    const url, icon: string, icon = null; // TODO assign real value

    const wrapper = render(<LinkIcon url, icon={url, icon} />);
    expect(wrapper).toMatchSnapshot();
  });
});
