/* tslint:disable no-var-requires */
const withReadme = (require("storybook-readme/with-readme") as any).default;
const LinkIconReadme = require("./README.md");

import * as React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import LinkIcon from "./LinkIcon";

storiesOf("LinkIcon", module)
  .addDecorator(withReadme(LinkIconReadme))
  .add("default", () => {
    const url, icon: string, icon = null; // TODO assign real value

    return (
      <LinkIcon url, icon={url, icon} />
    );
  });
