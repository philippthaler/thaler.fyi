import * as React from "react";
import { SemanticICONS, Icon, Popup, Button } from "semantic-ui-react";

interface LinkIconProps {
  url: string;
  icon: SemanticICONS;
  circular: boolean;
}

const style = {
  background: "#282c34",
  borderRadius: 0,
  color: "white",
  fontWeight: "bold",
  opacity: 0.9,
  padding: "1em",
};

const LinkIcon = ({ url, icon, circular }: LinkIconProps) => {
  return (
    <Popup
      on="hover"
      content={url}
      style={style}
      position="bottom left"
      trigger={
        <Button
          as="a"
          href={url}
          icon={<Icon name={icon} />}
          circular={circular}
        ></Button>
      }
    />
  );
};

export default LinkIcon;
