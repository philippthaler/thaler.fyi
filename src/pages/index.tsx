import * as React from "react";
import { Link } from "gatsby";
import HeaderMenu from "../components/HeaderMenu/HeaderMenu";
import { withLayout, LayoutProps, menuItems } from "../components/Layout";
import {
  Button,
  Segment,
  Container,
  Grid,
  Header,
  Icon,
  Card,
  Popup,
} from "semantic-ui-react";
import DynamicGridColumn from "../components/DynamicGridColumn/DynamicGridColumn";

const IndexPage = (props: LayoutProps) => (
  <div>
    {/* Master head */}
    <Segment vertical inverted textAlign="center" className="masthead">
      <HeaderMenu
        Link={Link}
        pathname={props.location.pathname}
        items={menuItems}
        inverted
      />
      <Container text>
        <Header inverted as="h1">
          thaler.fyi
        </Header>
        <Header inverted as="h2">
          Personal Website of Philipp Thaler
        </Header>
      </Container>
    </Segment>

    {/* About this starter */}
    <Segment vertical className="stripe">
      <Grid stackable verticalAlign="middle" className="container">
        <Grid.Row>
          <Grid.Column width="8">
            <Header>Lorem ipsum</Header>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro
              laudantium ad, quae, perspiciatis ipsa distinctio.
            </p>
            <Header>Dolor sit amet</Header>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro
              laudantium ad, quae, perspiciatis ipsa distinctio.
            </p>
          </Grid.Column>
          <Grid.Column width="6" floated="right">
            {/* TODO replace with a pretty GIF */}
            <Header>Lorem ipsum</Header>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro
              laudantium ad, quae, perspiciatis ipsa distinctio.
            </p>
            <Header>Dolor sit amet</Header>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Porro
              laudantium ad, quae, perspiciatis ipsa distinctio.
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>

    {/* Key features */}
    <Segment vertical className="stripe alternate feature">
      <Grid
        columns="3"
        textAlign="center"
        divided
        relaxed
        stackable
        className="container"
      >
        <Grid.Row>
          <DynamicGridColumn
            iconName="file code outline"
            title="My dotfiles"
            link="https://github.com/philippthaler/dotfiles"
            content={
              <p>
                Sharing my setup for Manjaro i3. My dotfiles consist of
                configuration files for i3, (Neo)vim, Polybar, Qutebrowser,
                ranger, termite and Zsh.
              </p>
            }
          />

          <DynamicGridColumn
            iconName="wizard"
            content={
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptas eaque at quae cupiditate aspernatur quibusdam!
                Distinctio quod non, harum dolorum earum molestias, beatae
                expedita aliquam dolorem asperiores nemo amet quaerat.
              </p>
            }
          />
          <DynamicGridColumn
            iconName="wizard"
            content={
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptas eaque at quae cupiditate aspernatur quibusdam!
                Distinctio quod non, harum dolorum earum molestias, beatae
                expedita aliquam dolorem asperiores nemo amet quaerat.
              </p>
            }
          />
        </Grid.Row>
      </Grid>
    </Segment>
  </div>
);

export default withLayout(IndexPage);
